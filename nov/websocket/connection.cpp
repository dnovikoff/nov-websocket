#include "connection.hpp"

#include <websocketpp/config/asio_no_tls.hpp>
#include <websocketpp/server.hpp>

namespace Nov {
namespace Websocket {

Connection::Connection() {
}

Connection::Connection(WebsocketServer* server, const Handler& handler)
	: server_(server), handler_(handler) {
}

Connection::Connection(Connection&&) = default;
Connection& Connection::operator=(Connection&&) = default;

Connection::~Connection() {
	cancelTimer();
}

void Connection::cancelTimer() {
	if (timer_) {
		timer_->cancel();
	}
}

void Connection::setTimeout() {
	cancelTimer();
	using std::placeholders::_1;
	timer_ = server_->set_timer(5000, std::bind(&Self::onTimeout, this, _1));
}

void Connection::initTimeout(long pingIntervalMs) {
	pingWhenIdleForMs_ = pingIntervalMs;
	setTimeout();
}

void Connection::close() {
	server_->close(handler_,
		websocketpp::close::status::normal, "Closed by user");
}

void Connection::onTimeout(const Error& e) {
	if (e || handler_.expired()) {
		return;
	}
	server_->ping(handler_, "");
}

void Connection::sendText(const std::string& data) {
	server_->send(handler_, data, websocketpp::frame::opcode::TEXT);
	setTimeout();
}

void Connection::sendBinary(const std::string& data) {
	server_->send(handler_, data, websocketpp::frame::opcode::BINARY);
	setTimeout();
}

} // namespace Websocket
} // namespace Nov
