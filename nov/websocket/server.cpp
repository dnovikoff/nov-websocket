#include "server.hpp"

#include <websocketpp/config/asio_no_tls.hpp>
#include <websocketpp/server.hpp>

namespace Nov {
namespace Websocket {

Server::Server(const ServerConfiguration& config, boost::asio::io_service* io, UserHandler* handler): handler_(handler), config_(config) {
	init();
	server_->init_asio(io);
	start(config.port);
}

Server::~Server() {
}

Connection* Server::findByHandler(const Handler& handler) {
	auto i = connections_.find(handler);
	if (i == connections_.end()) {
		return nullptr;
	}
	return &i->second;
}

void Server::start(int port) {
	server_->listen(port);
	server_->start_accept();
}

void Server::stop() {
	server_->stop();
}

void Server::init() {
	server_.reset(new WebsocketServer);
	server_->set_access_channels(websocketpp::log::alevel::all);
	server_->clear_access_channels(
		websocketpp::log::alevel::frame_payload);
	server_->set_pong_timeout(config_.pongTimeoutMs);
	server_->set_close_handshake_timeout(config_.closeHandshakeTimeoutMs);
	server_->set_open_handshake_timeout(config_.openHandshakeTimeoutMs);
	bindHandlers();
}

void Server::bindHandlers() {
	using std::placeholders::_1;
	using std::placeholders::_2;
	typedef WebsocketServer::message_ptr MessagePtr;

	server_->set_message_handler([this] (Handler handler, MessagePtr ptr) {
		switch(ptr->get_opcode()) {
		case (websocketpp::frame::opcode::TEXT):
			handler_->onTextMessage(handler, ptr->get_payload());
			break;
		case (websocketpp::frame::opcode::BINARY):
			handler_->onBinaryMessage(handler, ptr->get_payload());
			break;
		default:
			break;
		}
		setTimeout(handler);
	});

	server_->set_open_handler(std::bind(&Self::onOpen, this, _1));
	server_->set_ping_handler(std::bind(&Self::onPing, this, _1, _2));
	server_->set_pong_handler(std::bind(&Self::onPong, this, _1, _2));

	server_->set_close_handler(std::bind(&Self::onClose, this, _1));
	server_->set_pong_timeout_handler(std::bind(&Self::onTimeout, this, _1));
}

bool Server::onPing(Handler, const std::string&) {
	return true;
}

void Server::onPong(Handler handler, const std::string&) {
	setTimeout(handler);
}

void Server::setTimeout(Handler handler) {
	auto c = findByHandler(handler);
	if (!c) {
		return;
	}
	c->setTimeout();
}

void Server::onTimeout(Handler handler) {
	server_->close(handler, websocketpp::close::status::normal, "No pong responce");
}

void Server::onClose(Handler handler) {
	connections_.erase(handler);
	handler_->onClose(handler);
}

void Server::onOpen(Handler handler) {
	auto& c = connections_[handler];
	c = Connection(server_.get(), handler);
	handler_->onOpen(handler);
	c.initTimeout(config_.pingWhenIdleForMs);
}

void Server::broadcastText(const std::string& message) {
	for (auto& x: connections_) {
		x.second.sendText(message);
	}
}

void Server::broadcastBinary(const std::string& message) {
	for (auto& x: connections_) {
		x.second.sendBinary(message);
	}
}

} // namespace Websocket
} // namespace Nov
