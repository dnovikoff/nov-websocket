#ifndef NOV_WEBSOCKET_SERVER_HPP_
#define NOV_WEBSOCKET_SERVER_HPP_

#include <map>

#include <boost/asio/io_service.hpp>

#include <nov/websocket/connection.hpp>
#include <nov/websocket/fwd.hpp>

namespace Nov {
namespace Websocket {

class UserHandler {
public:
	typedef Websocket::Handler Handler;

	UserHandler () = default;
	virtual ~UserHandler() = default;

	virtual void onOpen(Handler handler) = 0;
	virtual void onClose(Handler handler) = 0;
	virtual void onBinaryMessage(Handler handler, const std::string&) = 0;
	virtual void onTextMessage(Handler handler, const std::string&) = 0;
};

class EmptyHandler: public UserHandler {
public:
	void onOpen(Handler) override {
	}
	void onClose(Handler) override {
	}
	void onBinaryMessage(Handler, const std::string&) override {
	}
	void onTextMessage(Handler, const std::string&) override {
	}
};

struct ServerConfiguration {
	long port = 9002;
	long openHandshakeTimeoutMs = 5000;
	long closeHandshakeTimeoutMs = 5000;
	long pongTimeoutMs = 5000;
	long pingWhenIdleForMs = 5000;
};

class Server {
public:
	explicit Server(const ServerConfiguration& config, boost::asio::io_service* io, UserHandler* handler);
	~Server();

	Connection* findByHandler(const Handler& handler);

	void stop();

	void broadcastText(const std::string& message);
	void broadcastBinary(const std::string& message);

	bool isEmpty() const {
		return connections_.empty();
	}

private:
	typedef Server Self;

	Server(const Server&) = delete;
	Server& operator=(const Server&) = delete;

	void start(int port);

	void init();
	void bindHandlers();
	bool onPing(Handler handle, const std::string& message);
	void onPong(Handler handle, const std::string& message);

	void setTimeout(Handler handle);
	void onTimeout(Handler handle);

	void onClose(Handler handle);
	void onOpen(Handler handle);

	std::unique_ptr<WebsocketServer> server_;
	UserHandler* handler_;
	std::map<Handler, Connection, std::owner_less<Handler> > connections_;
	ServerConfiguration config_;
};

} // namespace Websocket
} // namespace Nov

#endif // NOV_WEBSOCKET_SERVER_HPP_
