#ifndef NOV_WEBSOCKET_FWD_HPP_
#define NOV_WEBSOCKET_FWD_HPP_

#define _WEBSOCKETPP_CPP11_STL_
#define _WEBSOCKETPP_NULLPTR_TOKEN_ 0

#include <memory>
#include <system_error>
#include <boost/asio/deadline_timer.hpp>
#include <boost/system/error_code.hpp>

namespace websocketpp {
template <typename config> class server;
template <typename config> class client;

namespace config {
class asio;
} // namespace config
} // namespace websocketpp

namespace Nov {
namespace Websocket {

typedef websocketpp::server<websocketpp::config::asio> WebsocketServer;
typedef websocketpp::client<websocketpp::config::asio> WebsocketClient;
typedef std::weak_ptr<void> Handler;

typedef std::shared_ptr<boost::asio::deadline_timer> TimerPtr;
typedef std::error_code Error;

} // namespace Websocket
} // namespace Nov

#endif // NOV_WEBSOCKET_FWD_HPP_
