#ifndef NOV_WEBSOCKET_CONNECTION_HPP_
#define NOV_WEBSOCKET_CONNECTION_HPP_

#include <nov/websocket/fwd.hpp>

namespace Nov {
namespace Websocket {

class Connection final {
public:
	Connection();

	Connection(Connection&&);
	Connection& operator=(Connection&&);

	Connection(const Connection&) = delete;
	Connection& operator=(const Connection&) = delete;

	~Connection();

	void close();

	void sendText(const std::string& data);
	void sendBinary(const std::string& data);

private:
	friend class Server;
	typedef Connection Self;

	explicit Connection(WebsocketServer* server, const Handler& handler);

	void cancelTimer();
	void setTimeout();
	void initTimeout(long pingIntervalMs);
	void onTimeout(const Error& e);

	WebsocketServer* server_ = nullptr;
	TimerPtr timer_;
	Handler handler_;
	long pingWhenIdleForMs_ = 5000;
};

} // namespace Websocket
} // namespace Nov

#endif // NOV_WEBSOCKET_CONNECTION_HPP_
